# Zaidan

> My custom boilerplate inspired by [HTML5 Boilerplate][h5bp-url].

[![Licence][licence-badge]][licence-url]

[Project home][website] &bullet; [Documentation][documents]

Zaidan's a light-weight boilerplate for small web projects. It's inspired by the
[HTML5 Boilerplate][h5bp-url] project customised to my own personal needs. It
also uses [Normalize.css](https://necolas.github.io/normalize.css/).

## Installation

Simply clone it like this:

```sh
git clone git@gitlab.com:carolinealing/zaidan.git
```

## Usage

Just delete what you don't need and start coding!

## Licence

Released under MIT. See [LICENSE][licence-url] for more.

Coded with :heart: by [Caroline Åling][user-url].

[documents]: https://carolinesoftware.com/boilerplate
[h5bp-url]: https://html5boilerplate.com
[licence-badge]: https://img.shields.io/gitlab/license/carolinealing%2Fzaidan
[licence-url]: LICENSE.md
[user-url]: https://gitlab.com/carolinealing
[website]: https://carolinesoftware.com/boilerplate
